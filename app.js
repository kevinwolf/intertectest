(function () {

  // Utils module.
  var Utils = (function () {
    var get = function (url, cb) {
      var request = new XMLHttpRequest();
      request.open('GET', url, true);

      request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
          var data = JSON.parse(request.responseText);
          cb(data);
        }
      };

      request.send();
    };

    return {
      get: get
    }
  })();

  // App module.
  var App = (function () {
    // Screens.
    var $listScreen = document.getElementById('listScreen')
      , $detailScreen = document.getElementById('detailScreen');

    // Initial users list.
    var Users = [];

    // Initialize the application.
    var init = function () {
      navigateToList();
      fetchUsers();
    };

    // Navigate to list screen.
    var navigateToList = function () {
      $listScreen.style.display = 'block';
      $detailScreen.style.display = 'none';

      // Bind listeners.
      var $newUserInput = document.getElementById('newUser')
        , $newUserButton = document.getElementById('addUser')
        , $resetUsers = document.getElementById('resetUsers');

      $newUserButton.onsubmit = function (e) {
        e.preventDefault();
        var userName = $newUserInput.value.trim();
        $newUserInput.value = '';

        if (!userName.length) {
          console.log('invalid')
          return false;
        }

        Users.push({
          name: userName,
          posts: []
        });

        renderUsers();
      };

      $resetUsers.onclick = function () {
        Users = [];
        renderUsers();
        fetchUsers();
      };
    };

    // Navigate to detail screen.
    var navigateToDetail = function (index) {
      $listScreen.style.display = 'none';
      $detailScreen.style.display = 'block';

      // Bind listeners.
      var $returnToList = document.getElementById('returnToList');
      $returnToList.onclick = navigateToList;

      // Render the detail page.
      var $userDetailName = document.getElementById('userDetailName')
        , $userDetailPosts = document.getElementById('userDetailPosts');

      $userDetailName.innerHTML = Users[index].name;
      if (Users[index].username) $userDetailName.innerHTML += ' (' + Users[index].username + ')'

      if (!Users[index].posts.length) {
        $userDetailPosts.innerHTML = 'This user has not posts';
        return;
      }

      Users[index].posts.forEach(function (post) {
        var $userPost = document.createElement('li');
        $userPost.innerHTML = post.id + '. ' + post.title;
        $userDetailPosts.appendChild($userPost);
      });
    };

    // Fetch users.
    var fetchUsers = function () {
      // Call the endpoint to populate the users list.
      Utils.get('http://jsonplaceholder.typicode.com/users', function (users) {
        Users = users;

        // Call the endpoint to retreive posts.
        Utils.get('http://jsonplaceholder.typicode.com/posts', function (posts) {
          posts.forEach(function (post) {
            Users.forEach(function (user) {
              if (user.id === post.userId) {
                if (!user.posts) user.posts = [];
                user.posts.push(post);
              }
            })
          });

          renderUsers();
        });
      });
    };

    // Render users.
    var renderUsers = function () {
      // DOM elements.
      var $usersList = document.getElementById('usersList')
        , $usersTotal = document.getElementById('usersTotal');

      $usersList.innerHTML = '';

      // Iterate the users array to add the item to the DOM.
      Users.forEach(function (User, index) {
        var $userElement = document.createElement('li')
          , $userInput = document.createElement('input')
          , $userDeleteButton = document.createElement('button')
          , $userDetailButton = document.createElement('button');

        $userInput.setAttribute('type', 'text');
        $userInput.setAttribute('value', User.name + ' (' + User.posts.length + ' post' + (User.posts.length === 1 ? '' : 's') + ')');
        $userElement.appendChild($userInput);

        $userDeleteButton.innerHTML = 'Delete';
        $userDeleteButton.onclick = deleteUser.bind(null, index);
        $userElement.appendChild($userDeleteButton);

        $userDetailButton.innerHTML = 'Details';
        $userDetailButton.onclick = navigateToDetail.bind(null, index);
        $userElement.appendChild($userDetailButton);

        $usersList.appendChild($userElement);
      });

      // Refresh the users count.
      $usersTotal.innerHTML = Users.length;
    };

    // Delete a user.
    var deleteUser = function (index) {
      Users.splice(index, 1);
      renderUsers();
    };

    return {
      init: init
    }
  })();

  // Starts the app.
  App.init();
})();
